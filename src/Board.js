import React, { Component } from 'react'
// import Blank from './BlankPiece'
// import X from './XPiece'
// import O from './OPiece'


export default class extends Component{


    render() {


        return(
        <div className="ttt">
            <div className="row">
                <div className="square" id="top-left">O</div>
                <div className="square" id="top-mid"></div>
                <div className="square" id="top-right">O</div>
            </div>
            <div className="row">
                <div className="square" id="center-left"></div>
                <div className="square" id="center-mid">X</div>
                <div className="square" id="center-right"></div>
            </div>
            <div className="row">
                <div className="square" id="bottom-left">X</div>
                <div className="square" id="bottom-mid">O</div>
                <div className="square" id="bottom-right">X</div>
            </div>
        </div>
        )
    }

}

